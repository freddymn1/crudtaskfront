import React, {Component} from 'react';
// Componentes
import Header from './componentes/Header'
// Redux
import { Provider } from 'react-redux'
// Routing
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
// Store
import store from './store'
// Componentes
import Tareas from './componentes/Tareas';
import NuevoTarea from './componentes/NuevoTarea';
import EditarTarea from './componentes/EditarTarea';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
         <Router>
           <React.Fragment>
             <Header />
             <div className="container">
               <Switch>
                 <Route exact path="/" component={Tareas} />
                 <Route exact path="/tareas/nuevo" component={NuevoTarea} />
                 <Route exact path="/tareas/editar/:id" component={EditarTarea} />
               </Switch>
             </div>
           </React.Fragment>
         </Router>
      </Provider>
    );
  }
}

export default App;
