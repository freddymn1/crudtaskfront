import { MOSTRAR_TAREAS, ELIMINAR_TAREA, AGREGAR_TAREA, EDITAR_TAREA } from '../actions/types'

// cada reducers tiene su propio state
const initialState = {
    tareas: []
}

// los reducers son los unicos que pueden modificar el store
export default function (state = initialState, action) {
    switch (action.type) {
        case MOSTRAR_TAREAS:
            return {
                ...state,
                tareas: action.payload
            }
        case ELIMINAR_TAREA:
            return {
                ...state,
                tareas: state.tareas.filter(tarea=> tarea.id !== action.id)
            }
        case AGREGAR_TAREA:
            return {
                ...state,
                tareas: [...state.tareas, action.payload]
            }
        case EDITAR_TAREA:
            console.log(action.payload);
            return {
                ...state,
                tareas: state.tareas.map(tarea =>
                    tarea.id === action.payload ? ( tarea= action.payload) : tarea 
                )
            }
        default:
            return state;
    }
}