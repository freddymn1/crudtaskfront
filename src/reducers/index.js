import { combineReducers } from 'redux'
import tareasReducers from './tareasReducers' 

export default combineReducers({
    tareas: tareasReducers
});