import { MOSTRAR_TAREAS, ELIMINAR_TAREA, AGREGAR_TAREA, EDITAR_TAREA } from './types';

import axios from 'axios';

export const mostrarTareas = () => async dispatch => {
     const respuesta = await axios.get('http://localhost:8080/listar/tarea');
     dispatch({
          type: MOSTRAR_TAREAS,
          payload: respuesta.data.payload.data.ltask
          
     })
}

export const borrarTarea = id => async dispatch => {
     const respuesta =await axios.delete(`http://localhost:8080/eliminar/tarea/${id}`);
     console.log(id);
     dispatch({
          type: ELIMINAR_TAREA,
          payload: respuesta.data,
          id : id
     })
}

export const agregarTarea = producto => async dispatch => {
     const respuesta = await axios.post('http://localhost:8080/registrar/tarea', producto);
     dispatch({
          type: AGREGAR_TAREA,
          payload: respuesta.data
     })
}

export const editarTarea = producto => async dispatch => {
     console.log("producto =" + producto);
     const respuesta = await axios.put(`http://localhost:8080/actualiza/tarea/${producto.id}`, producto);
     dispatch({
          type: EDITAR_TAREA,
          payload: respuesta.data
     })
}