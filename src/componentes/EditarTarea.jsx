import React, { Component } from 'react';


// Redux
import {connect} from 'react-redux';
import { editarTarea } from '../actions/tareasActions';

class EditarTarea extends Component {

     state = { 
          id:'',
          description: '',
          creationDate: '',
          active: '',
          error: false
     }
     componentDidMount() {
         const { id,description } = this.props.match.params;

         this.props.editarTarea(id,description);
     }
     componentWillReceiveProps(nextProps, nextState){

        const {description, creationDate,active,id} = nextProps.tarea;

        this.setState({
          description,
          creationDate,
          active,
          id
        });

     }
     
     descriptionTask = e => {
          this.setState({description: e.target.value })
     }

     creationDateTask = e => {
          this.setState({creationDate: e.target.value })
     }

     actualizarTarea = e => {
          e.preventDefault();

          const { description, creationDate, active } = this.state;
          
          if(description === '' || creationDate === '') {
               this.setState({error: true});
               return;
          } 
          this.setState({error: false});

          // tomar el ID
          const { id } = this.props.match.params;

          // Crear el objeto
          const infoTarea = {
              id,
              description,
              creationDate,
              active
          }
          //   console.log(infoTarea);
          this.props.editarTarea(infoTarea);

          // redireccionar
          this.props.history.push('/');
     }

     render() { 
          const {description, creationDate, error} = this.state;
          return ( 
                <div className="row justify-content-center mt-5">
                    <div className="col-md-8">
                         <div className="card">
                         <div className="card-body">
                              <h2 className="text-center">Agregar Nuevo Tarea</h2>
                              <form onSubmit={this.actualizarTarea}>
                                   <div className="form-group">
                                        <label>Description</label>
                                        <input defaultValue={description} onChange={this.descriptionTask} type="text" className="form-control" placeholder="Description" />
                                   </div>
                                   <div className="form-group">
                                        <label>creationDate</label>
                                        <input defaultValue={creationDate} onChange={this.creationDateTask}  type="text" className="form-control" placeholder="creationDate" />
                                   </div>
                                   <button type="submit" className="btn editar p-2 font-weight-bold text-uppercase d-block w-100">Guardar Cambios</button>
                              </form>
                              {error ? 
                                   <div className="font-weight-bold alert alert-danger text-center mt-4 ">
                                        Todos los campos son Obligatorios
                                   </div> 
                                   : ''
                              }
                         </div>
                         </div>
                    </div>
               </div>
           );
     }
}
// state
const mapStateToProps = state => ({
    tarea: state.tareas.tarea
})
 
export default connect(mapStateToProps, {editarTarea})(EditarTarea);