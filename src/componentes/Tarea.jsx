import React, { Component } from 'react';


import { Link } from 'react-router-dom';

// Redux
import {connect} from 'react-redux';
import { borrarTarea } from '../actions/tareasActions';

class Tarea extends Component {
     
     eliminarTarea = () => {
          const {id} = this.props.info;
          this.props.borrarTarea(id);
     }

     render() { 
          const { id, description, creationDate,active} = this.props.info;
          return ( 
               <li className="list-group-item">
                    <div className="row justify-content-between align-items-center">
                         <div className="col-md-8 d-flex justify-content-between align-items-center">
                              
                            <p className="m-0">
                                   {id}
                              </p>
                              <p className="m-0">
                                   {description}
                              </p>
                              <p className="m-0">
                              {creationDate}</p>

                              <p className="m-0">
                                   {active}
                              </p>
                         </div>
                         <div className="col-md-4 d-flex justify-content-end acciones">
                              <Link to={`tareas/editar/${id}`} className="btn editar mr-2">Editar</Link>
                              <button onClick={this.eliminarTarea} type="button" className="btn eliminar">Borrar</button>
                         </div>
                    </div>
               </li>
           );
     }
}
 
export default connect(null, {borrarTarea})(Tarea);