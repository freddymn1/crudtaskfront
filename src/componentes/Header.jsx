import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Header extends Component {

     render() { 
          return ( 
               <nav className="navbar navbar-expand-lg navbar-dark bg-info justify-content-between d-flex">
                    <h1>
                         <Link to={'/'} className="text-light">CRUD Redux</Link>
                    </h1>

                    <Link to={'/tareas/nuevo'} className="btn btn-danger nuevo-post">
                         Agregar Tarea
                    </Link>
               </nav>
           );
     }
}
 
export default Header;