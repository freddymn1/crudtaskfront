import React, { Component } from 'react';


// Redux
import {connect} from 'react-redux';
import { agregarTarea } from '../actions/tareasActions';

class NuevoTarea extends Component {

     state = { 

          id:'',
          description: '',
          creationDate: '',
          active: '',
          error: false
     }
     
     nombreTarea = e => {
          this.setState({description: e.target.value })
     }

     creationDate = e => {
          this.setState({creationDate: e.target.value })
     }

     idTarea= e =>{
      this.setState({id:e.target.value})

     }

     nuevoTarea = e => {
          e.preventDefault();

          const { description, creationDate, id } = this.state;
          
          if(description === '' || creationDate === '' || id ==='') {
               this.setState({error: true});
               return;
          } 
          this.setState({error: false})

          // Crear el objeto
          const infoTarea = {
               description,
              creationDate,
              id
          }
           console.log(infoTarea);
          // crear el nuevo tarea
          this.props.agregarTarea(infoTarea);

          // redireccionar
          this.props.history.push('/');
     }

     render() { 
          const {error} = this.state;
          return ( 
                <div className="row justify-content-center mt-5">
                    <div className="col-md-8">
                         <div className="card">
                         <div className="card-body">
                              <h2 className="text-center">Agregar Nuevo Tarea</h2>
                              <form onSubmit={this.nuevoTarea}>
                                     <div className="form-group">
                                        <label>id</label>
                                        <input onChange={this.idTarea} type="text" className="form-control" placeholder="IdTarea" />
                                   </div>
                                   
                                   
                                   <div className="form-group">
                                        <label>Description</label>
                                        <input onChange={this.nombreTarea} type="text" className="form-control" placeholder="Description" />
                                   </div>
                                   <div className="form-group">
                                        <label>creationDate</label>
                                        <input onChange={this.creationDate}  type="text" className="form-control" placeholder="creationDate" />
                                   </div>
                                   <button type="submit" className="btn editar p-2 font-weight-bold text-uppercase d-block w-100">Agregar</button>
                              </form>
                              {error ? 
                                   <div className="font-weight-bold alert alert-danger text-center mt-4 ">
                                        Todos los campos son Obligatorios
                                   </div> 
                                   : ''
                              }
                         </div>
                         </div>
                    </div>
               </div>
           );
     }
}
 
export default connect(null, {agregarTarea})(NuevoTarea);