import React, { Component } from 'react';
import Tarea from './Tarea';

// Redux
import { connect } from 'react-redux';
import { mostrarTareas } from '../actions/tareasActions';

class Tareas extends Component {

     componentDidMount() {
          this.props.mostrarTareas();
     }

     render() { 
          const {tareas} = this.props;
          return ( 
               <React.Fragment>
                    <h2 className="text-center productos-title my-5">Listado de Tareas</h2>
                    <div className="row justify-content-center">
                         <div className="col-md-8">
                              <ul className="lista">
                                   {tareas.map(tarea => (
                                        <Tarea
                                             key={tarea.id}
                                             info={tarea}
                                        />
                                   ))}
                              </ul>
                         </div>
                    </div>
               </React.Fragment>
           );
     }
}
// state
const mapStateToProps = state => ({
     tareas: state.tareas.tareas
})
 
export default connect(mapStateToProps, { mostrarTareas })(Tareas);